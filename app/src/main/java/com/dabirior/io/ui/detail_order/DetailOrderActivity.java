package com.dabirior.io.ui.detail_order;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.dabirior.io.R;
import com.dabirior.io.ui.discuss.DiscussActivity;
import com.dabirior.io.ui.payment.PaymentActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetailOrderActivity extends AppCompatActivity {

    @BindView(R.id.lin1)
    LinearLayout linearLayout1;

    @BindView(R.id.lin2)
    LinearLayout linearLayout2;

    @BindView(R.id.lin3)
    LinearLayout linearLayout3;

    @BindView(R.id.text_title)
    TextView textTitle;

    @BindView(R.id.image_clothes)
    ImageView imageClothes;

    @BindView(R.id.spinner_size)
    Spinner spinnerSize;

    String clothes, clothSize,material = "Katun";
    String size[] = {"S", "M", "L", "XL"};

    public static Intent start(Context context, String clothes){
        Intent intent = new Intent(context, DetailOrderActivity.class);
        intent.putExtra("clothes", clothes);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_order);
        ButterKnife.bind(this);

        clothes = getIntent().getStringExtra("clothes");
        textTitle.setText(clothes);
        if (clothes.equals("Model 1")) imageClothes.setImageResource(R.mipmap.model1);
        else if (clothes.equals("Model 2")) imageClothes.setImageResource(R.mipmap.model2);
        else if (clothes.equals("Model 3")) imageClothes.setImageResource(R.mipmap.model3);

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this,   android.R.layout.simple_spinner_item, size);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        spinnerSize.setAdapter(spinnerArrayAdapter);

        spinnerSize.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                clothSize = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                clothSize = "M";
            }
        });
    }

    @OnClick({R.id.lin1, R.id.lin2, R.id.lin3, R.id.button_pay, R.id.button_discuss})
    void onClicked(View view){
        int id = view.getId();
        switch (id){
            case R.id.lin1:
                linearLayout1.setBackgroundColor(Color.parseColor("#e9e9e9"));
                linearLayout2.setBackgroundColor(Color.parseColor("#ffffff"));
                linearLayout3.setBackgroundColor(Color.parseColor("#ffffff"));
                material = "Katun";
                break;
            case R.id.lin2:
                linearLayout1.setBackgroundColor(Color.parseColor("#ffffff"));
                linearLayout2.setBackgroundColor(Color.parseColor("#e9e9e9"));
                linearLayout3.setBackgroundColor(Color.parseColor("#ffffff"));
                material = "Polyester";
                break;
            case R.id.lin3:
                linearLayout1.setBackgroundColor(Color.parseColor("#ffffff"));
                linearLayout2.setBackgroundColor(Color.parseColor("#ffffff"));
                linearLayout3.setBackgroundColor(Color.parseColor("#e9e9e9"));
                material = "Fleece";
                break;
            case R.id.button_pay:
                startActivity(PaymentActivity.start(DetailOrderActivity.this, clothes, material, clothSize));
                finish();
                break;
            case R.id.button_discuss:
                DiscussActivity.start(DetailOrderActivity.this);
                break;
        }
    }
}
