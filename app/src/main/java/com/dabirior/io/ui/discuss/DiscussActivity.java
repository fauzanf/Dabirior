package com.dabirior.io.ui.discuss;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.dabirior.io.R;

public class DiscussActivity extends AppCompatActivity {

    public static void start(Context context){
        context.startActivity(new Intent(context, DiscussActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discuss);
    }
}
