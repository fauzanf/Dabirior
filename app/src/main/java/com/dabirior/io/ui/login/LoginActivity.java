package com.dabirior.io.ui.login;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.dabirior.io.R;
import com.dabirior.io.ui.main.MainActivity;
import com.dabirior.io.ui.main.MainBottom;
import com.dabirior.io.ui.register.RegisterActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {

    public static void start(Context context){
        context.startActivity(new Intent(context, LoginActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        com.orhanobut.hawk.Hawk.init(getApplicationContext())
                .setEncryption(new com.orhanobut.hawk.NoEncryption())
                .build();

    }

    @OnClick({R.id.button_register, R.id.button_login})
    public void onClicked(View view){
        int id = view.getId();

        switch (id){
            case R.id.button_register:
                RegisterActivity.start(LoginActivity.this);
                finish();
                break;

            case R.id.button_login:
                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                builder.setTitle(getResources().getString(R.string.app_name));
                builder.setCancelable(false);
                builder.setMessage("Anda harus daftar terlebih dahulu untuk masuk ke aplikasi ini");
                builder.setPositiveButton("Ya", (dialogInterface, i) -> RegisterActivity.start(LoginActivity.this));
                builder.setNegativeButton("Tidak", null);

                AlertDialog dialog = builder.create();
                dialog.show();
                break;
        }
    }
}
