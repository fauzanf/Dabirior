package com.dabirior.io.ui.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import com.dabirior.io.R;
import com.dabirior.io.ui.main.about.AboutFragment;
import com.dabirior.io.ui.main.home.HomeFragment;
import com.dabirior.io.ui.main.order.OrderFragment;
import com.dabirior.io.ui.main.profile.ProfileFragment;

public class MainBottom extends AppCompatActivity {

    private TextView mTextMessage;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment = null;
            FragmentManager fragmentManager = getSupportFragmentManager();

            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fragment = new HomeFragment();

                    fragmentManager
                            .beginTransaction()
                            .replace(R.id.fragment, fragment)
                            .addToBackStack(null)
                            .commit();
                    return true;
                case R.id.navigation_order:
                    fragment = new OrderFragment();

                    fragmentManager
                            .beginTransaction()
                            .replace(R.id.fragment, fragment)
                            .addToBackStack(null)
                            .commit();
                    return true;
                case R.id.navigation_help:
                    fragment = new AboutFragment();
                    fragmentManager
                            .beginTransaction()
                            .replace(R.id.fragment, fragment)
                            .commit();
                    return true;
                case R.id.navigation_profile:
                    fragment = new ProfileFragment();
                    fragmentManager
                            .beginTransaction()
                            .replace(R.id.fragment, fragment)
                            .commit();
                    return true;
            }


            return false;
        }
    };

    public static void start(Context context){
        context.startActivity(new Intent(context, MainBottom.class)
            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_bottom);

        mTextMessage = findViewById(R.id.message);
        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        BottomNavigationViewHelper.disableShiftMode(navigation);
        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) navigation.getLayoutParams();

        layoutParams.setBehavior(new BottomNavigationViewBehavior());

    }

}
