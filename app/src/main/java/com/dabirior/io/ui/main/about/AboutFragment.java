package com.dabirior.io.ui.main.about;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.dabirior.io.R;

import java.util.Objects;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutFragment extends Fragment {


    public AboutFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_about, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @OnClick({R.id.card_about, R.id.card_suggestion})
    void onClicked(View view){
        int id = view.getId();
        switch (id){
            case R.id.card_about:
                AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getContext()));
                builder.setTitle(getResources().getString(R.string.app_name));
                builder.setCancelable(false);
                builder.setMessage("Tentang Kami\n\n\n" +
                        "Dabirior adalah pusat menjahit dengan wawasan fashion tren global. Disini kami menyediakan produk yang bermutu " +
                        "serta desainer yang berkualitas khususnya didaerah Bandung dengan navigasi yang simple dan intuitif.\n\n" +
                        "Sistem navigasi kami memastikan Anda dapat melakukan pesanan dengan cepat dan mudah\n\n" +
                        "Berbagai macam metode pembayaran yang mudah dan aman : \n" +
                        "Debit / Kredit\n\n" +
                        "Setelah anda memesan, Anda akan mendapatkan Email masuk.");
                builder.setPositiveButton("Ok", (dialog, i) -> dialog.dismiss());

                AlertDialog dialog = builder.create();
                dialog.show();
                break;
            case R.id.card_suggestion:
                LayoutInflater layoutInflaterAndroid = LayoutInflater.from(getContext());
                @SuppressLint("InflateParams") final View mView = layoutInflaterAndroid.inflate(R.layout.layout_suggestion, null);
                AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(Objects.requireNonNull(getContext()));
                alertDialogBuilderUserInput.setView(mView);

                ImageView close = mView.findViewById(R.id.imgClose);
                TextView nameText = mView.findViewById(R.id.nameText);
                Button buttonSend = mView.findViewById(R.id.button);


                final AlertDialog dialogInterface = alertDialogBuilderUserInput.create();

                close.setOnClickListener(v -> dialogInterface.dismiss());
                buttonSend.setOnClickListener(view1 -> dialogInterface.dismiss());
                dialogInterface.show();
                break;
        }
    }

}
