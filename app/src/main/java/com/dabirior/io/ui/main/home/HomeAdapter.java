package com.dabirior.io.ui.main.home;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.dabirior.io.R;
import com.dabirior.io.ui.detail_order.DetailOrderActivity;

import java.util.ArrayList;
import java.util.List;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder> {

    private Context context;
    private List<String> list;

    public HomeAdapter(Context context, List<String> list){
        this.list = new ArrayList<>();
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public HomeAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_clothes, parent, false);

        // create ViewHolder
        return new ViewHolder(itemLayoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeAdapter.ViewHolder holder, int position) {
        holder.textTitle.setText(list.get(position));
        if (position == 0) holder.imageItem.setImageResource(R.mipmap.model1);
        else if (position == 1) holder.imageItem.setImageResource(R.mipmap.model2);
        else holder.imageItem.setImageResource(R.mipmap.model3);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageItem;
        TextView textTitle;
        Button buttonBuy;

        public ViewHolder(View itemView) {
            super(itemView);
            imageItem = itemView.findViewById(R.id.image_clothes);
            textTitle = itemView.findViewById(R.id.text_title);
            buttonBuy = itemView.findViewById(R.id.button_buy);

            itemView.setOnClickListener(view -> {
                int position = getAdapterPosition();
                String clothes = list.get(position);
                context.startActivity(DetailOrderActivity.start(context, clothes));
            });

            buttonBuy.setOnClickListener(view -> {
                int position = getAdapterPosition();
                String clothes = list.get(position);
                context.startActivity(DetailOrderActivity.start(context, clothes));
            });
        }
    }
}
