package com.dabirior.io.ui.main.profile;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.dabirior.io.R;
import com.dabirior.io.ui.login.LoginActivity;

import java.util.Objects;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {


    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @OnClick({R.id.button_logout, R.id.button_profile})
    void onClicked(View view){
        int id = view.getId();
        switch (id){
            case R.id.button_logout:
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle(getResources().getString(R.string.app_name));
                builder.setCancelable(false);
                builder.setMessage("Apakah anda yakin ingin logout ? ");
                builder.setPositiveButton("Ya", (dialogInterface, i) -> LoginActivity.start(Objects.requireNonNull(getContext())));
                builder.setNegativeButton("Tidak", null);

                AlertDialog dialog = builder.create();
                dialog.show();
                break;

            case R.id.button_profile:
                LayoutInflater layoutInflaterAndroid = LayoutInflater.from(getContext());
                @SuppressLint("InflateParams") final View mView = layoutInflaterAndroid.inflate(R.layout.layout_edit_profile, null);
                android.support.v7.app.AlertDialog.Builder alertDialogBuilderUserInput = new android.support.v7.app.AlertDialog.Builder(Objects.requireNonNull(getContext()));
                alertDialogBuilderUserInput.setView(mView);

                ImageView close = mView.findViewById(R.id.image_close);
                Button buttonSend = mView.findViewById(R.id.button_submit);


                final android.support.v7.app.AlertDialog dialogInterface = alertDialogBuilderUserInput.create();

                close.setOnClickListener(v -> dialogInterface.dismiss());
                buttonSend.setOnClickListener(view1 -> dialogInterface.dismiss());
                dialogInterface.show();
                break;
        }
    }

}
