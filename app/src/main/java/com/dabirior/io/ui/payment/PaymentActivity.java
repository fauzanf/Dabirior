package com.dabirior.io.ui.payment;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dabirior.io.R;
import com.dabirior.io.ui.detail_order.DetailOrderActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PaymentActivity extends AppCompatActivity {

    @BindView(R.id.image_clothes)
    ImageView imageClothes;

    @BindView(R.id.text_title)
    TextView textTitle;

    @BindView(R.id.text_material)
    TextView textMaterial;

    @BindView(R.id.text_size)
    TextView textSize;

    String clothes, material, size;

    public static Intent start(Context context, String clothes, String material, String size){
        Intent intent = new Intent(context, PaymentActivity.class);
        intent.putExtra("clothes", clothes);
        intent.putExtra("material", material);
        intent.putExtra("size", size);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        ButterKnife.bind(this);

        clothes = getIntent().getStringExtra("clothes");
        material = getIntent().getStringExtra("material");
        size = getIntent().getStringExtra("size");

        if (clothes.equals("Model 1")) imageClothes.setImageResource(R.mipmap.model1);
        else if (clothes.equals("Model 2")) imageClothes.setImageResource(R.mipmap.model2);
        else if (clothes.equals("Model 3")) imageClothes.setImageResource(R.mipmap.model3);

        textTitle.setText(clothes);
        textMaterial.setText(material);
        textSize.setText(size);
    }

    @OnClick({R.id.button_pay, R.id.img_back})
    void onClicked(View view){
        int id = view.getId();
        switch (id){
            case R.id.button_pay:
                AlertDialog.Builder builder = new AlertDialog.Builder(PaymentActivity.this);
                builder.setTitle(getResources().getString(R.string.app_name));
                builder.setCancelable(false);
                builder.setMessage("pembayaran sukses terima kasih sudah menggunakan Dibirior");
                builder.setPositiveButton("Ok", (dialogInterface, i) -> finish());

                AlertDialog dialog = builder.create();
                dialog.show();
                break;
            case R.id.img_back:
                startActivity(DetailOrderActivity.start(PaymentActivity.this, clothes));
                finish();
                break;
        }
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        startActivity(DetailOrderActivity.start(PaymentActivity.this, clothes));
        finish();
    }
}
