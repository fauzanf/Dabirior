package com.dabirior.io.ui.register;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.dabirior.io.R;
import com.dabirior.io.ui.login.LoginActivity;
import com.dabirior.io.ui.main.MainBottom;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends AppCompatActivity {

    public static void start(Context context){
        context.startActivity(new Intent(context, RegisterActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.button_register)
    void onClicked(View view){
        MainBottom.start(RegisterActivity.this);
        finish();
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        LoginActivity.start(RegisterActivity.this);
        finish();
    }
}
